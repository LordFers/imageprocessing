# ImageProcessing-RealTimeSystems

![Imagen](https://gitlab.com/LordFers/imageprocessing/-/raw/main/background.jpg)

## About this project
Liquid Level Detector System using Image Processing for Real Time Systems (Sistemas de Tiempo Real) at UNTREF.
Liquid Level Detector System is a C++14 application implementing a liquid level detection algorithm based on the paper "New Algorithm of Liquid Level of Infusion Bottle Based on Image Processing." The application is native and cross-platform, with retrocompatibility for UNIX systems. It adheres to SOLID principles, designed in a modular and polymorphic way for easy extensibility.

## Key Features
- **Detection Algorithm:** Custom implementation and Canny Edge Detector from OpenCV.
- **Cross-platform:** Entirely native in C++14 with UNIX system support.
- **Object-Oriented:** Code structured according to SOLID principles.
- **Extensible Interfaces:** Allows extension of detection algorithms, rendering, display and more.
- **OpenGL Rendering:** Real-time rendering using OpenGL fixed-function (1.2).
- **Graphical Interface:** GUI implementation with ImGUI.
- **Image Capture:** Real-time with sensors and camera simulation using OpenCV.
- **Liquid Level Detection:** Based on the algorithm from the mentioned paper.
- **Log Registration:** Avoids main thread blocking with I/O resource locking.
- **Console:** Integration of a console for additional interaction.
- **Capture Saving:** Functionality for storing captures.
- **UI Design and Implementation:** Design and implementation of 3/4 Windows: Diffuse Capture, Canny Edge Capture, Console, MessageBox.

## External Dependencies
- OpenCV
- GLFW
- ImGUI

## Compilation Instructions

1. Clone the repository: `git clone https://gitlab.com/LordFers/imageprocessing.git`
2. Navigate to the project directory: `cd Liquid-Level-Detector-System`
3. Create a `build` directory: `mkdir build && cd build`
4. Configure the project with CMake: `cmake ..`
5. Compile the project: `make`
6. Run the resulting application.

## TO DO
- **Task Manager:** Task circuit management.
- **Separate Threads:** Division of threads for improved efficiency.
- **Video Source Selection:** List of camera and/or sensor devices.
- **More Detection Algorithms:** Greater variety and performance comparisons.

## Authors and acknowledgment
Fernando Quinteros - Legajo: 39422

## Contributions
Contributions are welcome. Feel free to fork and send a pull request.

## License
![image info](https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Public_Domain_Mark_button.svg/1200px-Public_Domain_Mark_button.svg.png)

This project is public domain and is not under any License. Feel free to do whatever you want with it.

## Project status
In-progress.
