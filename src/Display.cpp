#include "Display.h"
#include "imgui.h"
#include "backends/imgui_impl_opengl2.h"
#include "backends/imgui_impl_glfw.h"
#include <GLFW/glfw3.h>

Display::Display(LiquidLevelDetectorSystem* system)
	: m_System(system), m_Window(nullptr)
{

}

Display::~Display()
{
	delete m_System;
}

bool Display::Start()
{
	if (!glfwInit())
	{
		std::cerr << "GLFW couldn't initialize." << std::endl;
		return false;
	}

	auto glfw_error_callback = [](int error, const char* description) {
		fprintf(stderr, "Error %d: %s\n", error, description);
	};

	glfwSetErrorCallback(glfw_error_callback);

	m_Window = glfwCreateWindow(1920, 1080, "Liquid Level Detector - Sistemas de Tiempo Real", NULL, NULL);
	if (!m_Window) {
		glfwTerminate();
		return 1;
	}
	glfwMakeContextCurrent(m_Window);

	// ImGui Initialization:
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui_ImplGlfw_InitForOpenGL(m_Window, true);
	ImGui_ImplOpenGL2_Init();

	if (m_System->Init())
	{
		while (!glfwWindowShouldClose(m_Window))
		{
			glfwPollEvents();
			m_System->Process();
			glfwSwapBuffers(m_Window);
		}

		m_System->Close();
	}

	return true;
}
