#include "LiquidLevelDetectorAlgorithm.h"

LiquidLevelDetectorAlgorithm::LiquidLevelDetectorAlgorithm()
    : m_LiquidLevel(NULL)
{

}

void LiquidLevelDetectorAlgorithm::Process(cv::Mat& frame, ILiquidLevelDetectorParameters* parameters)
{
    if (frame.empty())
    {
        std::cerr << "Frame couldn't be captured." << std::endl;
        return;
    }

    CannyLiquidLevelDetectorParameters* cannyParameters = static_cast<CannyLiquidLevelDetectorParameters*>(parameters);
    
    if (!cannyParameters)
        return;

    // Preprocessing image:
    cv::cvtColor(frame, frame, cv::COLOR_BGR2GRAY);
    const int avgVal = m_ImageProcessor.GetAveragePixelValue(frame);
    m_ImageProcessor.AdjustThreshold(frame, avgVal, static_cast<int16_t>(cannyParameters->range));

    cannyParameters->aperture = 5; //we need force this in the meantime due to some other issues related to edge and threshold computation
    cv::Canny(frame, frame, cannyParameters->t1, cannyParameters->t2, static_cast<int>(cannyParameters->aperture), cannyParameters->gradient);
    cv::cvtColor(frame, frame, cv::COLOR_GRAY2BGR); //We need this due to some issues with OpenGL and sending Texture with a invalid format.
    m_LiquidLevel = m_ImageProcessor.CalculateLiquidLevel(frame);
}
