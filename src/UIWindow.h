#pragma once

#include "imgui.h"

class UIWindow
{
public:
	UIWindow(const char* title, int width, int height, int x_pos, int y_pos);
	virtual ~UIWindow() = default;

public:
	void Draw();
	void UpdateBackgroundTexture(ImTextureID imTexture, int width, int height);

public:
	inline int GetWindowPosX() const { return m_WindowPosX; }
	inline int GetWindowPosY() const { return m_WindowPosY; }
	inline int GetWidth() const { return m_Width; }
	inline int GetHeight() const { return m_Height; }

private:
	virtual void DrawInternally();
	void ShowMessageBox(const char* title, const char* message);
	void EmptyRectangle(const ImVec2& size);

protected:
	void MessageBox(const char* title, const char* message);

private:
	const char* m_Title;
	int m_Width;
	int m_Height;
	int m_WindowPosX;
	int m_WindowPosY;
	bool m_DirtyBackground;
	bool m_IsMsgBoxOpened;
	const char* m_MsgBoxTitle;
	const char* m_MsgBoxMsg;

protected:
	ImTextureID m_ImTextureBackgroundID;
	int m_WidthTextureBackground;
	int m_HeightTextureBackground;
};
