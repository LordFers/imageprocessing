#pragma once

#include "UIWindow.h"

class CannyUIWindow : public UIWindow
{
public:
	CannyUIWindow(const char* title, int width, int height, int x_pos, int y_pos);
	virtual ~CannyUIWindow() = default;

private:
	virtual void DrawInternally() override;
};
