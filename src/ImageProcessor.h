#pragma once

#include <opencv2/opencv.hpp>

class ImageProcessor {
public:
	void PreprocessFrame(cv::Mat& frame);
	int GetAveragePixelValue(cv::Mat& img);
	int CalculateLiquidLevel(cv::Mat& img);
	void AdjustThreshold(cv::Mat& img, int16_t avgVal, int16_t range);
};