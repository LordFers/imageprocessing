#pragma once

#include "UIWindow.h"
#include <vector>
#include <string>

class ConsoleUIWindow : public UIWindow
{
public:
	ConsoleUIWindow(const char* title, int width, int height, int x_pos, int y_pos);
	virtual ~ConsoleUIWindow() = default;

private:
	virtual void DrawInternally() override;
};
