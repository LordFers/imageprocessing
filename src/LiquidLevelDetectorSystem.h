#pragma once

#include "LiquidLevelDetectorAlgorithm.h"

class Display;
class IRenderer;

class LiquidLevelDetectorSystem {
public:
	static constexpr const char* LOCAL_URL = "http://192.168.100.2:4747/mjpegfeed?640x480";
	static constexpr const char* SCREENSHOTS_PATH = "Screenshots/";

public:
	virtual ~LiquidLevelDetectorSystem();

protected:
	LiquidLevelDetectorSystem();
	static LiquidLevelDetectorSystem* m_Instance;

public:
	static LiquidLevelDetectorSystem* GetInstance()
	{
		if (m_Instance == nullptr)
		{
			m_Instance = new LiquidLevelDetectorSystem();
		}

		return m_Instance;
	}

public:
	bool Init();
	void Process();
	void Close();
	bool SaveCurrentFrame(const std::string& filename);
	bool OpenCamera(const std::string& URL = LOCAL_URL);
	inline void SetMaxLiquidLevel(int16_t level) { m_MaxLiquidLevel = level; }

public:
	inline ILiquidLevelDetectorParameters* GetDetectorParameters() const { return m_DetectorParameters; }

private:
	void DrawLevelLine();
	void CheckLiquidLevel();

private:
	IRenderer* m_Renderer;
	ILiquidLevelDetectorAlgorithm* m_DetectorAlgorithm;
	ILiquidLevelDetectorParameters* m_DetectorParameters;
	cv::VideoCapture m_Capture;
	cv::Mat m_CurrentFrame;
	cv::Mat m_CurrentFrameUnprocessed;
	int16_t m_MaxLiquidLevel;
	std::string m_CurrentURL;
	bool m_IsCameraOpen;
};
