#pragma once

typedef void* RenderElement;

enum class RenderableObjectType
{
	None,
	DiffuseCapture,
	CannyCapture,
	LastObject
};

struct RenderObject {
	RenderElement element;
	RenderableObjectType objType;
};

class IRenderer
{
public:
	virtual void OnCreate() = 0;
	virtual void OnUpdate(double delta) = 0;
	virtual void OnAddElement(RenderObject renderObj) = 0;
	virtual void OnDelete() = 0;
};
