#include "Logger.h"

Logger* Logger::m_Instance = nullptr;
std::mutex Logger::m_Mutex;

Logger::Logger()
{
    m_LogFile.open("output_logs.txt", std::ios::app);
}

Logger::~Logger()
{
    if (m_LogFile.is_open())
    {
        m_LogFile.close();
    }
}

void Logger::AddLog(const std::string& message)
{
    std::lock_guard<std::mutex> lock(m_Mutex);
    if (m_LogFile.is_open())
    {
        m_LogFile << message << std::endl;
        std::cout << message << std::endl;
        m_Logs.push_back(message);
    }
}
