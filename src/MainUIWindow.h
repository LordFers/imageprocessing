#pragma once

#include "UIWindow.h"

class MainUIWindow : public UIWindow
{
public:
	MainUIWindow(const char* title, int width, int height, int x_pos, int y_pos);
	virtual ~MainUIWindow() = default;

private:
	virtual void DrawInternally() override;
};
