#include "LiquidLevelDetectorSystem.h"
#include "RendererGL.h"
#include "Logger.h"

LiquidLevelDetectorSystem* LiquidLevelDetectorSystem::m_Instance = nullptr;

LiquidLevelDetectorSystem::LiquidLevelDetectorSystem()
	: m_DetectorAlgorithm(new LiquidLevelDetectorAlgorithm()), m_DetectorParameters(new CannyLiquidLevelDetectorParameters()), m_Renderer(new RendererGL()), m_MaxLiquidLevel(NULL), m_IsCameraOpen(false)
{
	//
}

LiquidLevelDetectorSystem::~LiquidLevelDetectorSystem()
{
	delete m_Renderer;
	delete m_DetectorAlgorithm;
}

bool LiquidLevelDetectorSystem::Init()
{
	OpenCamera();
	m_Renderer->OnCreate();
	return true;
}

// Process sample
void LiquidLevelDetectorSystem::Process()
{
	if (m_IsCameraOpen)
	{
		m_Capture >> m_CurrentFrame;
		m_CurrentFrameUnprocessed = m_CurrentFrame.clone();
		m_DetectorAlgorithm->Process(m_CurrentFrame, m_DetectorParameters);
		DrawLevelLine();
		CheckLiquidLevel();
	}

	// TODO:
	// We need separate OpenCV thread from Render Thread here. (We won't wait OpenCV thread,
	// we can use a double buffer to avoid issues on capture OpenCV 'currentframe' in the next OpenGL frame)
	// ...

	m_Renderer->OnAddElement(RenderObject{ &m_CurrentFrame, RenderableObjectType::CannyCapture });
	m_Renderer->OnAddElement(RenderObject{ &m_CurrentFrameUnprocessed, RenderableObjectType::DiffuseCapture });
	m_Renderer->OnUpdate(0.0f);
}

void LiquidLevelDetectorSystem::DrawLevelLine()
{
	const int16_t liquidLevel = m_DetectorAlgorithm->GetLiquidLevel();
	cv::line(m_CurrentFrame, cv::Point(0, liquidLevel), cv::Point(m_CurrentFrame.cols, liquidLevel), cv::Scalar(0, 0, 255, 255), 2);
	cv::line(m_CurrentFrameUnprocessed, cv::Point(0, m_MaxLiquidLevel), cv::Point(m_CurrentFrame.cols, m_MaxLiquidLevel), cv::Scalar(255, 128, 128, 255), 2);
}

void LiquidLevelDetectorSystem::CheckLiquidLevel()
{
	if (m_MaxLiquidLevel <= m_DetectorAlgorithm->GetLiquidLevel())
	{
		// TODO: Here we must send a signal to the embedded system in charge of moving the bottles.
		// SendSignal(...)
		// We can add more details to log (something like a log type to recognize using colors)
		Logger::GetInstance()->AddLog("A bottle filled has been detected successfully.");
		m_MaxLiquidLevel = 0;
	}
}

void LiquidLevelDetectorSystem::Close()
{
	// Save last frame:
	SaveCurrentFrame("lastcannyframe.jpg");
	
	// Release frames:
	m_CurrentFrame.release();
	m_CurrentFrameUnprocessed.release();

	// Release capture:
	m_Capture.release();

	// Delete Renderer:
	m_Renderer->OnDelete();
}

bool LiquidLevelDetectorSystem::SaveCurrentFrame(const std::string& filename)
{
	if (!m_IsCameraOpen)
		return false;

	// We need have created the directory before:
	//mkdir(SCREENSHOTS_PATH);
	cv::imwrite(std::string(SCREENSHOTS_PATH) + "canny_" + filename, m_CurrentFrame);
	cv::imwrite(std::string(SCREENSHOTS_PATH) + "diffuse_" + filename, m_CurrentFrameUnprocessed);

	return true;
}

bool LiquidLevelDetectorSystem::OpenCamera(const std::string& URL)
{
	if (m_Capture.isOpened())
		return true; // When we have a device list, we can release and delete the capture device and create/open it again.

	m_Capture.open(URL);
	// Sometimes it doesn't return open the 'OpenCV device' but in the next tick it's opened, so, that's why I did it.
	if (!m_Capture.isOpened())
	{
		std::cerr << "Camera couldn't be opened." << std::endl;
		return false;
	}

	m_Capture.set(cv::CAP_PROP_BUFFERSIZE, 2);
	m_IsCameraOpen = true;
	m_CurrentURL = URL;

	return true;
}
