#pragma once

#include "Renderer.h"
#include "MainUIWindow.h"
#include "CannyUIWindow.h"
#include "ConsoleUIWindow.h"
#include <queue>

typedef unsigned int GLuint;

struct GLElement {
	RenderObject renderObj;
	GLuint textureID;
};

class RendererGL : public IRenderer
{
public:
	virtual void OnCreate() override;
	virtual void OnUpdate(double delta) override;
	virtual void OnAddElement(RenderObject renderObj) override;
	virtual void OnDelete() override;

public:
	ConsoleUIWindow* GetConsoleUIWindow() const { return m_ConsoleWindow; }

private:
	void DrawElements();
	void PrepareGLElement(GLElement& element, int16_t& width, int16_t& height);

private:
	std::queue<GLElement> m_ElementsToDraw;
	std::vector<GLuint> m_TexturesToDelete;
	MainUIWindow* m_MainWindow;
	CannyUIWindow* m_LiquidLevelWindow;
	ConsoleUIWindow* m_ConsoleWindow;
};
