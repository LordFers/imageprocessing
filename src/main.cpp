#include "Display.h"

int main()
{
    Display display(LiquidLevelDetectorSystem::GetInstance());
    if (!display.Start())
    {
        std::cerr << "Display can't be initialized" << std::endl;
    }

    return 0;
}
