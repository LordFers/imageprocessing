#include "MainUIWindow.h"
#include <GLFW/glfw3.h>
#include <opencv2/opencv.hpp>
#include "LiquidLevelDetectorSystem.h"
#include "Logger.h"

MainUIWindow::MainUIWindow(const char* title, int width, int height, int x_pos, int y_pos)
	: UIWindow(title, width, height, x_pos, y_pos)
{
	//
}

void MainUIWindow::DrawInternally()
{
    static float maxLiquidLevelPer = 0.0f;
    float invertedValue = 1.0f - maxLiquidLevelPer;

    ImGui::SetCursorPos(ImVec2(660.0f, 28.0f));
    if (ImGui::VSliderFloat(" Max Liquid Level:", ImVec2(16, 100), &invertedValue, 0.0f, 1.0f, ""))
    {
        maxLiquidLevelPer = 1.0f - invertedValue;
    }

    ImGui::SetCursorPos(ImVec2(808.0f, 32.0f));
    ImGui::Text("%.3f", maxLiquidLevelPer);

    ImGui::SetCursorPos(ImVec2(686.0f, 48.0f));
    if (ImGui::Button("Open Camera"))
    {
        // TODO: A device selector (list) to use when call open camera:
        if (!LiquidLevelDetectorSystem::GetInstance()->OpenCamera(/* ... */))
        {
            MessageBox("Error", "Error trying to open your capture device.");
        }
        else
        {
            Logger::GetInstance()->AddLog("Camera open.");
            //if (m_ImTextureBackgroundID)
                //MessageBox("Warning", "Warning: You already have a device open.");
        }
    }

    LiquidLevelDetectorSystem::GetInstance()->SetMaxLiquidLevel(static_cast<int16_t>(maxLiquidLevelPer * 480.0f));
}
