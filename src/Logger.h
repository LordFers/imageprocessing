#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <mutex>
#include <vector>

class Logger {
private:
    Logger();

public:
    ~Logger();

    static Logger* GetInstance()
    {
        std::lock_guard<std::mutex> lock(m_Mutex);
        if (m_Instance == nullptr)
        {
            m_Instance = new Logger();
        }
        return m_Instance;
    }

public:
    void AddLog(const std::string& message);
    inline const std::vector<std::string>& GetLogs() const { return m_Logs; }

private:
    std::vector<std::string> m_Logs;
    std::ofstream m_LogFile;
    static Logger* m_Instance;
    static std::mutex m_Mutex;
};
