#pragma once

#include "LiquidLevelDetectorSystem.h"

struct GLFWwindow;
class Display
{
public:
	Display(LiquidLevelDetectorSystem* system);
	~Display();

public:
	bool Start();

private:
	LiquidLevelDetectorSystem* m_System;
	struct GLFWwindow* m_Window;
};
