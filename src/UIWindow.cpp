#include "UIWindow.h"
#include "Logger.h"
#include <ImGUI/imgui_internal.h>

UIWindow::UIWindow(const char* title, int width, int height, int x_pos, int y_pos)
	: m_Title(title), m_Width(width), m_Height(height), m_DirtyBackground(false), m_WindowPosX(x_pos), m_WindowPosY(y_pos),
	m_WidthTextureBackground(NULL), m_HeightTextureBackground(NULL), m_ImTextureBackgroundID(nullptr), m_IsMsgBoxOpened(false),
	m_MsgBoxTitle(""), m_MsgBoxMsg("")
{

}

void UIWindow::Draw()
{
	ImGui::SetNextWindowPos(ImVec2(m_WindowPosX, m_WindowPosY));
	ImGui::SetNextWindowSize(ImVec2(m_Width, m_Height));
	ImGui::Begin(m_Title, NULL, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse);

	if (m_DirtyBackground)
	{
		if (m_ImTextureBackgroundID && m_WidthTextureBackground > 0 && m_HeightTextureBackground > 0)
		{
			ImGui::Image(m_ImTextureBackgroundID, ImVec2(static_cast<float>(m_WidthTextureBackground), static_cast<float>(m_HeightTextureBackground)));
		}
		else
		{
			EmptyRectangle(ImVec2(640.0f, 480.0f));
		}

		m_DirtyBackground = false;
	}

	DrawInternally();

	if (m_IsMsgBoxOpened)
		ShowMessageBox(m_MsgBoxTitle, m_MsgBoxMsg);

	ImGui::End();
}

void UIWindow::UpdateBackgroundTexture(ImTextureID imTexture, int width, int height)
{
	m_ImTextureBackgroundID = imTexture;
	m_WidthTextureBackground = width;
	m_HeightTextureBackground = height;
	m_DirtyBackground = true;
}

void UIWindow::DrawInternally()
{
	// Empty implementation
}

void UIWindow::MessageBox(const char* title, const char* message)
{
	m_IsMsgBoxOpened = true;
	m_MsgBoxTitle = title;
	m_MsgBoxMsg = message;

	Logger::GetInstance()->AddLog("[MessageBox - " + std::string(title) + "]: " + message);
}

void UIWindow::ShowMessageBox(const char* title, const char* message)
{
	const ImVec2& screenSize = ImGui::GetIO().DisplaySize;
	ImGui::SetNextWindowPos(ImVec2(screenSize.x * 0.45f, screenSize.y * 0.45f), ImGuiCond_Appearing);
	ImGui::OpenPopup(title);
	if (ImGui::BeginPopupModal(title, NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		ImGui::Text("%s", message);
		if (ImGui::Button("Accept"))
		{
			m_IsMsgBoxOpened = false;
			ImGui::CloseCurrentPopup();
		}
		ImGui::EndPopup();
	}
}

void UIWindow::EmptyRectangle(const ImVec2& size)
{
	ImGuiWindow* window = ImGui::GetCurrentWindow();
	if (window == nullptr)
		return;

	ImDrawList* drawList = ImGui::GetWindowDrawList();
	if (drawList == nullptr)
		return;

	// White shape
	ImVec2 p0 = window->DC.CursorPos;
	ImVec2 p1 = ImVec2(p0.x + size.x, p0.y + size.y);
	ImU32 color = IM_COL32(255, 255, 255, 255);

	drawList->AddRectFilled(p0, p1, color);
}
