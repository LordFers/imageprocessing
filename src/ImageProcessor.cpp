#include "ImageProcessor.h"

void ImageProcessor::PreprocessFrame(cv::Mat& frame)
{
    // TODO:
}

int ImageProcessor::GetAveragePixelValue(cv::Mat& img)
{
    int sum = 0;
    int count = 0;

    for (int i = 0; i < img.rows; i++)
    {
        for (int j = 0; j < img.cols; j++)
        {
            int pixelValue = img.at<uchar>(i, j);
            if (pixelValue != 0 && pixelValue != 255)
            {
                sum += pixelValue;
                count++;
            }
        }
    }

    return count > 0 ? sum / count : 0;
}

int ImageProcessor::CalculateLiquidLevel(cv::Mat& img)
{
    int16_t liquidLevel = -1;
    int16_t maxWhitePixelCount = 0;
    int16_t verticalRange = 2;
    int16_t ignoreBottomRegion = 16;

    for (int row = 0; row < img.rows; row++)
    {
        int whitePixelCount = 0;

        for (int col = 0; col < img.cols; col++)
        {
            // Avoid processing bottom and top regions:
            if (row <= ignoreBottomRegion || row >= img.rows - ignoreBottomRegion)
                break;

            // White pixel count by row
            if (img.at<uchar>(row, col) == 255)
                whitePixelCount++;
        }

        // Update liquid level to the most likely:
        if (whitePixelCount > maxWhitePixelCount)
        {
            maxWhitePixelCount = whitePixelCount;
            liquidLevel = row;
        }
    }

    // If we didn't find liquid, set level = -1
    if (maxWhitePixelCount == 0)
    {
        liquidLevel = -1;
    }

    return liquidLevel;
}

void ImageProcessor::AdjustThreshold(cv::Mat& img, int16_t avgVal, int16_t range)
{
    for (int i = 0; i < img.rows; i++)
    {
        for (int j = 0; j < img.cols; j++)
        {
            int pixelValue = img.at<uchar>(i, j);
            if (pixelValue > avgVal - range && pixelValue < avgVal + range)
            {
                img.at<uchar>(i, j) = 113;
            }
        }
    }
}
