#pragma once

#include "ImageProcessor.h"

struct ILiquidLevelDetectorParameters {
	// empty
};

struct CannyLiquidLevelDetectorParameters : public ILiquidLevelDetectorParameters {
	CannyLiquidLevelDetectorParameters()
		: t1(300.0), t2(600.0), gradient(true), aperture(5), range(5)
	{

	}

	double t1;
	double t2;
	bool gradient;
	uint8_t aperture;
	uint8_t range;
};

// We can implement different level detector algorithms with different approaches.
class ILiquidLevelDetectorAlgorithm
{
public:
	virtual void Process(cv::Mat& frame, ILiquidLevelDetectorParameters* parameters) = 0;
	virtual int16_t GetLiquidLevel() const = 0;
};

// Canny Algorithm: (maybe we need to change class name to CannyLiquidLevelDetectorAlgorithm in the future as parameter struct name)
class LiquidLevelDetectorAlgorithm : public ILiquidLevelDetectorAlgorithm
{
public:
	LiquidLevelDetectorAlgorithm();
	virtual ~LiquidLevelDetectorAlgorithm() = default;

public:
	virtual void Process(cv::Mat& frame, ILiquidLevelDetectorParameters* parameters) override;
	virtual inline int16_t GetLiquidLevel() const override { return m_LiquidLevel; }

protected:
	ImageProcessor m_ImageProcessor;
	int16_t m_LiquidLevel;
};
