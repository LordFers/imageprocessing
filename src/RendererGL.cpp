#include "RendererGL.h"
#include "imgui.h"
#include "backends/imgui_impl_opengl2.h"
#include "backends/imgui_impl_glfw.h"
#include <opencv2/opencv.hpp>
#include <GLFW/glfw3.h>

void RendererGL::OnCreate()
{
    m_MainWindow = new MainUIWindow("Capture", 960, 544, 0, 0);
    m_LiquidLevelWindow = new CannyUIWindow("Liquid Level Detector", 960, 544, m_MainWindow->GetWidth() + m_MainWindow->GetWindowPosX(), 0);
    m_ConsoleWindow = new ConsoleUIWindow("Console", 1920, 544, 0, m_MainWindow->GetHeight());
}

void RendererGL::OnUpdate(double delta)
{
    ImGui_ImplOpenGL2_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    DrawElements();
    ImGui::Render();

    // Rendering on GL viewport:
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());

    // Clean textures
    glDeleteTextures(static_cast<GLsizei>(m_TexturesToDelete.size()), &m_TexturesToDelete[0]);
}

void RendererGL::DrawElements()
{
    m_TexturesToDelete.clear();

    while (!m_ElementsToDraw.empty())
    {
        auto& elementToDraw = m_ElementsToDraw.front();
        int16_t texWidth, texHeight;
        PrepareGLElement(elementToDraw, texWidth, texHeight);
        ImTextureID imTexID = (ImTextureID)(intptr_t)elementToDraw.textureID;

        // We need improve this but I have no time:
        switch (elementToDraw.renderObj.objType)
        {
        case RenderableObjectType::DiffuseCapture:
        {
            m_MainWindow->UpdateBackgroundTexture(imTexID, texWidth, texHeight);
            m_MainWindow->Draw();
        }
        break;

        case RenderableObjectType::CannyCapture:
        {
            m_LiquidLevelWindow->UpdateBackgroundTexture(imTexID, texWidth, texHeight);
            m_LiquidLevelWindow->Draw();
        }
        break;

        default:
            break;
        }

        m_TexturesToDelete.push_back(elementToDraw.textureID);
        m_ElementsToDraw.pop();
    }

    m_ConsoleWindow->Draw();
}

void RendererGL::PrepareGLElement(GLElement& element, int16_t& width, int16_t& height)
{
    cv::Mat* frame = static_cast<cv::Mat*>(element.renderObj.element);
    if (frame && !frame->empty())
    {
        width = frame->cols;
        height = frame->rows;

        glBindTexture(GL_TEXTURE_2D, element.textureID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, frame->ptr());
    }
}

void RendererGL::OnAddElement(RenderObject renderObj)
{
    GLElement gl_element;
    gl_element.renderObj = renderObj;
    glGenTextures(1, &gl_element.textureID);
	m_ElementsToDraw.push(gl_element);
}

void RendererGL::OnDelete()
{
    delete m_MainWindow;
    delete m_LiquidLevelWindow;
}
