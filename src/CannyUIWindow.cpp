#include "CannyUIWindow.h"
#include "LiquidLevelDetectorSystem.h"
#include <chrono>

CannyUIWindow::CannyUIWindow(const char* title, int width, int height, int x_pos, int y_pos)
	: UIWindow(title, width, height, x_pos, y_pos)
{
	//
}

bool SaveCaptureUsingDate()
{
    auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    struct tm timeinfo;

    if (localtime_s(&timeinfo, &now) == 0)
    {
        char buffer[80];
        strftime(buffer, sizeof(buffer), "%Y-%m-%d %H_%M_%S", &timeinfo);
        return LiquidLevelDetectorSystem::GetInstance()->SaveCurrentFrame(std::string(buffer) + ".jpg");
    }

    return false;
}

void CannyUIWindow::DrawInternally()
{
    static float t1 = 300.0;
    static float t2 = 600.0;
    static int range = 5;
    static int aperture = 5;
    static bool gradient = true;
    static bool isL2 = true;

    ImGui::SetCursorPos(ImVec2(660.0f, 28.0f));
    ImGui::VSliderFloat(" First Canny Threshold:", ImVec2(16, 100), &t1, 0.0f, 1000.0f, "");
    ImGui::SetCursorPos(ImVec2(842.0f, 32.0f));
    ImGui::Text("%.3f", t1);

    ImGui::SetCursorPos(ImVec2(660.0f, 136.0f));
    ImGui::VSliderFloat(" Second Canny Threshold:", ImVec2(16, 100), &t2, 0.0f, 1000.0f, "");

    ImGui::SetCursorPos(ImVec2(850.0f, 140.0f));
    ImGui::Text("%.3f", t2);

    ImGui::SetCursorPos(ImVec2(660.0f, 248.0f));
    ImGui::VSliderInt(" Range Canny value:", ImVec2(16, 100), &range, 0, 10, "");

    ImGui::SetCursorPos(ImVec2(820.0f, 252.0f));
    ImGui::Text("%d", range);

    ImGui::SetCursorPos(ImVec2(660.0f, 356.0f));
    ImGui::VSliderInt(" Aperture Canny value:", ImVec2(16, 100), &aperture, 0, 10, "");

    ImGui::SetCursorPos(ImVec2(836.0f, 360.0f));
    ImGui::Text("%d", aperture);

    // Disabled -> L1 parameter enabled.
    ImGui::SetCursorPos(ImVec2(660.0f, 465.0f));
    ImGui::Checkbox("Check L2 gradient enabled", &isL2);

    ImGui::SetCursorPos(ImVec2(660.0f, 502.0f));
    if (ImGui::Button("Save capture"))
    {
        if (SaveCaptureUsingDate())
        {
            MessageBox("Screenshot:", "A current frame has been captured.");
        }
        else
        {
            MessageBox("Error:", "Current frame couldn't be saved.");
        }
    }

    // We need select this using RTTI or something in the future:
    CannyLiquidLevelDetectorParameters* cannyParameters = static_cast<CannyLiquidLevelDetectorParameters*>(LiquidLevelDetectorSystem::GetInstance()->GetDetectorParameters());
    cannyParameters->t1 = static_cast<double>(t1);
    cannyParameters->t2 = static_cast<double>(t2);
    cannyParameters->range = range;
    cannyParameters->aperture = aperture;
    cannyParameters->gradient = isL2;
}
