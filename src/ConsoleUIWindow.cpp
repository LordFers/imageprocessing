#include "ConsoleUIWindow.h"
#include "Logger.h"

ConsoleUIWindow::ConsoleUIWindow(const char* title, int width, int height, int x_pos, int y_pos)
	: UIWindow(title, width, height, x_pos, y_pos)
{

}

void ConsoleUIWindow::DrawInternally()
{
    const std::vector<std::string>& messages = Logger::GetInstance()->GetLogs();
    size_t count = 0;
    for (const auto& message : messages)
    {
        ImGui::Text("Log n� %d: %s", ++count, message.c_str());
    }
    ImGui::SetScrollHereY(1.0f);
}
